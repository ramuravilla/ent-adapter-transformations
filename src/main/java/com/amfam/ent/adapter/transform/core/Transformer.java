package com.amfam.ent.adapter.transform.core;

import com.amfam.ent.adapter.mapping.AdapterMappingConfigurer;
import com.amfam.ent.adapter.mapping.model.MappingResult;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface Transformer {

  public AdapterMappingConfigurer aConfigurer();

  public Object transform(String billData) throws Exception;

  /**
   * The first mapping file is for Parent Object
   * The rest are child objects, 
   * This supports inline mapping for child objects, then only parent mapping file is passed
   * @param billData
   * @param mappingFileName
   * @return
   * @throws Exception
   */
  default Object transformData(String billData, String... mappingFileName) throws Exception {
    ClassLoader classLoader = getClass().getClassLoader();
    InputStream inputStream = classLoader.getResourceAsStream(mappingFileName[0]);
    MappingResult mr = aConfigurer().doMapping(inputStream, billData);
    Object retObj = mr.getTransformedObject();
    Map<String, Object> objectMap = new HashMap<>();
    objectMap.put(mr.getParentClassName(), retObj);
    if (mappingFileName.length > 1) {
      for (int i = 1; i < mappingFileName.length; i++) {
        String mappingFileNm = mappingFileName[i];
        InputStream inputStream2 = classLoader.getResourceAsStream(mappingFileNm);
        MappingResult mr1 = aConfigurer().doMapping(inputStream2, billData);
        Object nestedObj = mr1.getTransformedObject();
        objectMap.put(nestedObj.getClass().getName(), nestedObj);

        Object assObj = objectMap.get(mr1.getParentClassName());

        Field nameField = assObj.getClass().getDeclaredField(mr1.getFieldName());
        nameField.setAccessible(true);
        /** handle arrays */
        String typeName = nameField.getType().getTypeName();
        if (typeName.endsWith("[]")) {
          String cName = typeName.substring(0, typeName.length() - 2);
          nameField.set(
              assObj,
              ((List) nestedObj)
                  .toArray(
                      (Object[])
                          Array.newInstance(Class.forName(cName), ((List) nestedObj).size())));
        } else {
          nameField.set(assObj, nestedObj);
        }
      }
    }
    return retObj;
  }
}
