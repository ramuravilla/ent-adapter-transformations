package com.amfam.ent.adapter.transform.core;

public interface SPELTransformer {
  String transform(String val);
}
