package com.amfam.ent.adapter.mapping;

import com.amfam.ent.adapter.mapping.model.MappingConfigBean;
import com.amfam.ent.adapter.mapping.model.MappingResult;
import com.amfam.ent.adapter.transform.core.SPELTransformer;
import com.amfam.ent.core.utils.JacksonUtils;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.type.MapType;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.expression.BeanFactoryResolver;
import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.stereotype.Component;

@Component
public class AdapterMappingConfigurer {

  private ApplicationContext appContext;

  public AdapterMappingConfigurer(ApplicationContext appContext) {
    this.appContext = appContext;
  }

  private static Log logger = LogFactory.getLog(AdapterMappingConfigurer.class);

  /**
   * Map source fields to tagrget fields target config , source data is passed by calling object The
   * Target bean should be generated from json schema
   *
   * @param mappingFileResource
   * @param jString
   * @return
   */
  public MappingResult doMapping(InputStream mappingFileResource, String jString) {
    MappingResult mr = null;
    try {

      Map<String, MappingConfigBean> mappingConfig =
          getMappingConfigFromMappingFile(mappingFileResource);
      MappingConfigBean context = mappingConfig.get("context");
      String className = context.getClassName();
      String nodeName = context.getNodeName();
      String fieldNameForNestedObj = context.getFieldName();
      String parentClassNameForNestedObj = context.getParentClassName();
      logger.info(
          "className "
              + className
              + " -->  nodeName "
              + nodeName
              + "  -------> fieldNameForNestedObj  "
              + fieldNameForNestedObj
              + "parentClassNameForNestedObj  "
              + parentClassNameForNestedObj);
      logger.info("jString -----> " + jString);
      JsonNode rootNode = getRootNode(nodeName, jString);

      if (rootNode.isArray()) {
        List<Object> retObjectList = new ArrayList<Object>();
        for (JsonNode jsonNode : rootNode) {
          retObjectList.add(doMapping2(className, mappingConfig, jsonNode));
        }
        mr = new MappingResult(fieldNameForNestedObj, retObjectList, parentClassNameForNestedObj);
      } else {
        Object retObj = doMapping2(className, mappingConfig, rootNode);
        mr = new MappingResult(fieldNameForNestedObj, retObj, parentClassNameForNestedObj);
      }

    } catch (Exception e) {
      logger.error("could not transform  " + jString);
      e.printStackTrace();
      throw new RuntimeException("Error while transforming , check the mapping config", e);
    }
    return mr;
  }
  /**
   * read mapping file, return a map with context and field definitions context key is context,
   * value is type MappingConfigBean field name is key for field definition, value is type
   * MappingConfigBean
   *
   * @param mappingFileResource
   * @return
   * @throws Exception
   */
  private Map<String, MappingConfigBean> getMappingConfigFromMappingFile(
      InputStream mappingFileResource) throws Exception {
    MapType cType =
        JacksonUtils.getJsonObjectMapper()
            .getTypeFactory()
            .constructMapType(Map.class, String.class, MappingConfigBean.class);
    Map<String, MappingConfigBean> mappingConfig =
        JacksonUtils.getJsonObjectMapper().readValue(mappingFileResource, cType);
    return mappingConfig;
  }
  /**
   * get input stream from mapping file
   *
   * @param mappingFileName
   * @return
   * @throws Exception
   */
  private InputStream getMappingFileResource(String mappingFileName) throws Exception {
    ClassLoader classLoader = getClass().getClassLoader();
    InputStream inputStream = classLoader.getResourceAsStream(mappingFileName);
    return inputStream;
  }
  /** 
   * get the root node from json string
   * @param nodeName
   * @param jString
   * @return
   * @throws Exception
   */
  private JsonNode getRootNode(String nodeName, String jString) throws Exception {
    JsonNode rootNode = null;
    if (nodeName != null && !nodeName.equals("")) {
      if (nodeName.indexOf("/") > -1) {
        rootNode = JacksonUtils.getJsonObjectMapper().readTree(jString).at(nodeName);
      } else {
        rootNode = JacksonUtils.getJsonObjectMapper().readTree(jString).get(nodeName);
      }
    } else {
      rootNode = JacksonUtils.getJsonObjectMapper().readTree(jString);
    }
    return rootNode;
  }

  /**
   * instantiate the object, process config mentioned in the mapping file
   *
   * @param className
   * @param mappingConfig
   * @param rootNode
   * @return
   * @throws Exception
   */
  public Object doMapping2(
      String className, Map<String, MappingConfigBean> mappingConfig, JsonNode rootNode)
      throws Exception {

    Class<?> clazz = Class.forName(className);
    Object retObj = clazz.newInstance();
    logger.info("class instantiated  " + clazz);

    mappingConfig.remove("context");

    /** if the json paths specified in the mapping config, hanlde the json paths */
    if (mappingConfig.entrySet().size() > 0) {

      handleCustomObject(retObj, mappingConfig, rootNode);
    } else {
      /** this is when the json node and the pojo match, no custom processing */
      retObj = JacksonUtils.getJsonObjectMapper().treeToValue(rootNode, clazz);
    }

    return retObj;
  }
  /**
   * handle the custome object specified in the mapping file
   *
   * @param retObj
   * @param mappingConfig
   * @param rootNode
   * @throws Exception
   */
  private void handleCustomObject(
      Object retObj, Map<String, MappingConfigBean> mappingConfig, JsonNode rootNode)
      throws Exception {
    StandardEvaluationContext sContext = new StandardEvaluationContext(retObj);
    sContext.setBeanResolver(new BeanFactoryResolver(appContext.getAutowireCapableBeanFactory()));
    ExpressionParser expressionParser = new SpelExpressionParser();

    Iterator<Map.Entry<String, MappingConfigBean>> itr = mappingConfig.entrySet().iterator();
    while (itr.hasNext()) {
      Map.Entry<String, MappingConfigBean> entry = itr.next();
      String fieldName = entry.getKey();
      MappingConfigBean config = entry.getValue();
      String from = config.getFrom();

      /**
       * @TODO check if from starts with / , if starts with / use json path expressopn with .at if
       * not use .get
       */
      JsonNode val_node = null;
      if(from != null) {
    	  val_node = rootNode.at(from);
      } else {
    	  /**
    	   * if from is not specified for a field, pass the rootNode for processing
    	   * this is when a wrapper object holds the result object
    	   */
    	  val_node = rootNode;
      }
      logger.info("fieldName '" + fieldName + "' -->  fieldValue " + val_node);
      if (val_node.isObject()) {
        handleObjectSpecifiedInMappingConfig(
            config, val_node, fieldName, sContext, expressionParser);
      } else if (val_node.isArray()) {
        handleListSpecifiedInMappingConfig(config, val_node, fieldName, sContext, expressionParser);
      } else {
        handleCustomPaths(config, val_node, fieldName, sContext, expressionParser);
      }
    }
  }
  /**
   * handle a list/array of nested objects mapping
   *
   * @param config
   * @param val_node
   * @param fieldName
   * @param sContext
   * @param expressionParser
   * @throws Exception
   */
  private void handleListSpecifiedInMappingConfig(
      MappingConfigBean config,
      JsonNode val_node,
      String fieldName,
      StandardEvaluationContext sContext,
      ExpressionParser expressionParser)
      throws Exception {
    MappingConfigBean context2 = config.getContext();
    if (context2 != null) {

      String mappingFileName = context2.getMappingFileName();
      String className = context2.getClassName();
      List<Object> retObjectList = new ArrayList<Object>();
      if (mappingFileName == null) {
        /**
         * this is when the context is specified in the mapping config for a field and no custom
         * mapping required
         */
        for (JsonNode jsonNode : val_node) {
          retObjectList.add(
              JacksonUtils.getJsonObjectMapper().treeToValue(jsonNode, Class.forName(className)));
        }
      } else {
    	  /**
           * this is when the context is specified in the mapping config for a field and custom
           * mapping required
           */
        InputStream mappingFileResource = getMappingFileResource(mappingFileName);
        Map<String, MappingConfigBean> mappingConfig =
            getMappingConfigFromMappingFile(mappingFileResource);
        for (JsonNode jsonNode : val_node) {
          retObjectList.add(doMapping2(className, mappingConfig, jsonNode));
        }
      }
      /**
       * handle array type
       */
      String typeName =
          expressionParser.parseExpression(fieldName).getValueType(sContext).getTypeName();
      if (typeName.endsWith("[]")) {
        String cName = typeName.substring(0, typeName.length() - 2);
        expressionParser
            .parseExpression(fieldName)
            .setValue(
                sContext,
                retObjectList.toArray(
                    (Object[]) Array.newInstance(Class.forName(cName), retObjectList.size())));
      } else {
        expressionParser.parseExpression(fieldName).setValue(sContext, retObjectList);
      }
    }
  }
  /**
   * assign an object (specified in the context in the mapping config) to the field (field name
   * specified in the context)
   *
   * @param config
   * @param val_node
   * @param fieldName
   * @param sContext
   * @param expressionParser
   * @throws Exception
   */
  private void handleObjectSpecifiedInMappingConfig(
      MappingConfigBean config,
      JsonNode val_node,
      String fieldName,
      StandardEvaluationContext sContext,
      ExpressionParser expressionParser)
      throws Exception {
    MappingConfigBean context2 = config.getContext();
    if (context2 != null) {

      String mappingFileName = context2.getMappingFileName();
      String className = context2.getClassName();
      Object nestedObj = null;
      if (mappingFileName == null) {
        /**
         * this is when the context is specified in the mapping config for a field and no custom
         * mapping required
         */
        nestedObj =
            JacksonUtils.getJsonObjectMapper().treeToValue(val_node, Class.forName(className));
      } else {
        /**
         * this is when the context is specified in the mapping config for a field and custom
         * mapping required
         */
        InputStream mappingFileResource = getMappingFileResource(mappingFileName);
        Map<String, MappingConfigBean> mappingConfig =
            getMappingConfigFromMappingFile(mappingFileResource);
        nestedObj = doMapping2(className, mappingConfig, val_node);
      }
      expressionParser.parseExpression(fieldName).setValue(sContext, nestedObj);
    }
  }
  /**
   * handle the custome paths specified in the mapping file
   *
   * @param config
   * @param val_node
   * @param fieldName
   * @param sContext
   * @param expressionParser
   * @throws Exception
   */
  private void handleCustomPaths(
      MappingConfigBean config,
      JsonNode val_node,
      String fieldName,
      StandardEvaluationContext sContext,
      ExpressionParser expressionParser)
      throws Exception {
    String val = val_node.asText();
    if (val == "null") {
    	val = null;
    }

    if (val != null && val.trim() != "") {

      String transformStr = config.getTransform();
      if (transformStr != null && transformStr != "") {
        if (transformStr.startsWith("@")) {
          SPELTransformer transformerBean =
              (SPELTransformer) appContext.getBean(transformStr.substring(1));
          String transformedVal = transformerBean.transform(val);
          logger.info("transformedVal   " + transformedVal);
          expressionParser.parseExpression(fieldName).setValue(sContext, transformedVal);
        } else {
          transformStr = "'" + val + "'." + transformStr;
          Expression expression = expressionParser.parseExpression(transformStr);
          String result = (String) expression.getValue();
          logger.info("transformedVal   " + result);
          expressionParser.parseExpression(fieldName).setValue(sContext, result);
        }
      } else {
        expressionParser.parseExpression(fieldName).setValue(sContext, val);
      }

    } else {
      expressionParser.parseExpression(fieldName).setValue(sContext, val);
    }
  }
}
