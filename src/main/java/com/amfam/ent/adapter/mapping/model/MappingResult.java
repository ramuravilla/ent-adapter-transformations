package com.amfam.ent.adapter.mapping.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class MappingResult {
  private String fieldName;
  private Object transformedObject;
  /** parent class name to assign to */
  private String parentClassName;
}
