package com.amfam.ent.adapter.mapping.model;

import lombok.Data;

@Data
public class MappingConfigBean {
  public MappingConfigBean() {
    // TODO Auto-generated constructor stub
  }

  /** class name with package */
  private String className;
  /** this is the parent node name in json */
  private String nodeName;
  /**
   * if context specified directly in the mapping file, then the child object is created from json
   * json paths and the pojo (in the class path) should match
   */
  private MappingConfigBean context;
  /** json path */
  private String from;
  /** SPEL statement used to transform value */
  private String transform;
  /** mapping file name */
  private String mappingFileName;
  /** used to assign nested objects to the parent */
  private String fieldName;
  /** parent class name to assign to */
  private String parentClassName;
}
