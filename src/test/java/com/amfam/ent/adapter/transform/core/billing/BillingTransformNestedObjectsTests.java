package com.amfam.ent.adapter.transform.core.billing;

import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;

import com.amfam.ent.adapter.mapping.AdapterMappingConfigurer;
import com.amfam.ent.model.billing.canonical.BillingAccount;

@SpringBootTest(
    classes = {
      BillingTransformer.class,
      BillingTransformerInlineCustom.class,
      BillingTransformerInlineNoCustom.class,
      BillingTransformerInlineNoCustomAndNestedNoInline.class,
      BillingTransformerCustomChangeParent.class,
      AdapterMappingConfigurer.class
    })
@ContextConfiguration
@AutoConfigureMockMvc(printOnlyOnFailure = false)
@EnableAutoConfiguration(
    exclude = {SecurityAutoConfiguration.class})
public class BillingTransformNestedObjectsTests {

  @Autowired MockMvc mockMvc;
  

  @Autowired BillingTransformer billingTransformer;
  @Autowired BillingTransformerInlineCustom billingTransformerInlineCustom;
  @Autowired BillingTransformerInlineNoCustom billingTransformerInlineNoCustom;
  @Autowired BillingTransformerInlineNoCustomAndNestedNoInline billingTransformerInlineNoCustomAndNestedNoInline;
  @Autowired BillingTransformerCustomChangeParent billingTransformerCustomChangeParent;

  private static Log logger = LogFactory.getLog(BillingTransformNestedObjectsTests.class);

  String pLoad =
      "{\"status\":{\"code\":200,\"reason\":\"OK\",\"messages\":[{\"code\":200000,\"description\":\"Access"
          + " to the requested resource was"
          + " successful.\",\"level\":\"SUCCESS\"}],\"metrics\":{\"acceptedOn\":\"2022-05-19T19:21:46.230Z\","
          + "\"returnedOn\":\"2022-05-19T19:21:46.912Z\"},\"maxMessageLevel\":\"INFO\",\"transactionId\":\"5B061CBC-22E6-FC4C-5021-DFB71FAC80DB\"},"
          + "\"billingAccount\":{\"accountNumber\":\"620-036-392-85\",\"accountStatus\":\"BILLING_OVERDUE\",\"accountType\":\"PERSONAL\","
          + "\"billingSystem\":\"BillingCenter\",\"currentAmountOwed\":{\"accountBalance\":199.72,\"creditBalance\":0,\"minimumDue\":199.72,\"fullPayBalance\":0},"
          + "\"isTPI\":false,\"partnerId\":\"536\",\"payDueDate\":\"04/30/2022\",\"policyList\":[{\"lineOfBusiness\":\"personal\",\"policyNumber\":\"41009-49909-91\","
          + "\"policyType\":\"FAMILY_CAR\",\"remainingBillableMonths\":0,\"underwritingCompany\":\"MIC\",\"primaryInsdFirstName\":\"Rvnvpaavdkdwabaoh\","
          + "\"primaryInsdLastName\":\"Ovzetkdd\",\"namedInsuredType\":\"PERSON\"}],\"producerCode\":275,\"unFormattedAccountNumber\":\"62003639285\","
          + "\"fullPayDiscountEligible\":false,\"metrics\":{\"acceptedOn\":\"2022-05-19T19:21:46.230Z\",\"returnedOn\":\"2022-05-19T19:21:46.912Z\"}}}";
  
  String pLoad_parentList =
	      "{\"status\":{\"code\":200,\"reason\":\"OK\",\"messages\":[{\"code\":200000,\"description\":\"Access"
	          + " to the requested resource was"
	          + " successful.\",\"level\":\"SUCCESS\"}],\"metrics\":{\"acceptedOn\":\"2022-05-19T19:21:46.230Z\","
	          + "\"returnedOn\":\"2022-05-19T19:21:46.912Z\"},\"maxMessageLevel\":\"INFO\",\"transactionId\":\"5B061CBC-22E6-FC4C-5021-DFB71FAC80DB\"},"
	          + "\"billingAccount\":[{\"accountNumber\":\"620-036-392-85\",\"accountStatus\":\"BILLING_OVERDUE\",\"accountType\":\"PERSONAL\","
	          + "\"billingSystem\":\"BillingCenter\",\"currentAmountOwed\":{\"accountBalance\":199.72,\"creditBalance\":0,\"minimumDue\":199.72,\"fullPayBalance\":0},"
	          + "\"isTPI\":false,\"partnerId\":\"536\",\"payDueDate\":\"04/30/2022\",\"policyList\":[{\"lineOfBusiness\":\"personal\",\"policyNumber\":\"41009-49909-91\","
	          + "\"policyType\":\"FAMILY_CAR\",\"remainingBillableMonths\":0,\"underwritingCompany\":\"MIC\",\"primaryInsdFirstName\":\"Rvnvpaavdkdwabaoh\","
	          + "\"primaryInsdLastName\":\"Ovzetkdd\",\"namedInsuredType\":\"PERSON\"}],\"producerCode\":275,\"unFormattedAccountNumber\":\"62003639285\","
	          + "\"fullPayDiscountEligible\":false,\"metrics\":{\"acceptedOn\":\"2022-05-19T19:21:46.230Z\",\"returnedOn\":\"2022-05-19T19:21:46.912Z\"}}]}";

  
  String pLoad_customer_search = "{\"contacts\":[{\"contactId\":\"31C3AC0\","
  		+ "\"sourceSystemIdentifiers\":[{\"sourceSystemId\":\"c17a10a7-06de-4f8c-8211-83e2a6057f07\",\"sourceSystemName\":\"CAH\"}],"
  		+ "\"firstName\":\"Talon\",\"middleName\":\"Q\",\"lastName\":\"McGlynn\",\"familySuffix\":null,\"professionalSuffix\":null,"
  		+ "\"dateOfBirth\":null,\"deceased\":false,\"driversLicenseNumber\":\"W43592788500\",\"driversLicenseState\":\"WI\","
  		+ "\"addresses\":[{\"addressType\":\"HOME\",\"primaryIndicator\":true,\"addressLine1\":\"2795 Kristoffer Stravenue\","
  		+ "\"addressLine2\":null,\"state\":\"Wisconsin\",\"city\":\"Madison\",\"zipCode\":\"53713\",\"zip4Code\":null,\"zip2Code\":null,"
  		+ "\"country\":\"United States\",\"longitude\":\"-158.3986\",\"latitude\":\"35.8404\"}],\"emails\":[{\"emailType\":null,"
  		+ "\"emailAddress\":\"Jovanny78@yahoo.com\",\"primaryIndicator\":true}],\"phones\":[{\"phoneType\":\"Home\","
  		+ "\"phoneNumber\":\"866-562-5613\",\"primaryIndicator\":false,\"extension\":null,\"specialInstructions\":\"\"}]},"
  		+ "{\"contactId\":\"31B5556\",\"sourceSystemIdentifiers\":[{\"sourceSystemId\":\"77\",\"sourceSystemName\":\"CAH\"}],"
  		+ "\"firstName\":\"Bulah\",\"middleName\":\"Q\",\"lastName\":\"McGlynn\",\"familySuffix\":null,\"professionalSuffix\":null,"
  		+ "\"dateOfBirth\":\"1970-01-01\",\"deceased\":false,\"driversLicenseNumber\":\"W43592788500\",\"driversLicenseState\":\"WI\","
  		+ "\"addresses\":[{\"addressType\":\"HOME\",\"primaryIndicator\":true,\"addressLine1\":\"2063 Homenick Views\","
  		+ "\"addressLine2\":null,\"state\":\"Wisconsin\",\"city\":\"Madison\",\"zipCode\":\"53713\",\"zip4Code\":null,"
  		+ "\"zip2Code\":null,\"country\":\"United States\",\"longitude\":\"78.7762\",\"latitude\":\"88.8145\"}],"
  		+ "\"emails\":[{\"emailType\":null,\"emailAddress\":\"Lavinia7@hotmail.com\",\"primaryIndicator\":true}],"
  		+ "\"phones\":[{\"phoneType\":\"Home\",\"phoneNumber\":\"244-988-3624\",\"primaryIndicator\":false,\"extension\":null,"
  		+ "\"specialInstructions\":\"\"}]}";
  
  @Test
  public void testTransformation_nestedObjects() throws Exception {
	  
	  /**
	   * mapping/billing/billingAccount_mapping.json
	   * mapping/billing/metrics_mapping.json
	   * mapping/billing/policy_mapping.json
	   * Parent Object is not a list, Parent and child mapping files are provided by the Transformer
	   * child mapping has information about the parent class and field in the parent class to be assigned to
	   * no inline mapping i.e parent mapping file has no child mapping 
	   * custom mapping (json paths specified in the mapping file and transformations at the field level) for parent
	   * custom mapping for child objects
	   * One child object is an array (policies)
	   */

	  Object retObj = billingTransformer.transform(pLoad);

    assertNotNull(retObj);
            assertNotNull(((BillingAccount) retObj).getMetrics());
            assertNotNull(((BillingAccount) retObj).getPolicies());
            System.out.println("TransformedObject ----> " + retObj);
         
  }
  
  @Test
  public void testTransformation_nestedObjects_inline_list_custom() throws Exception {
	  
	  /**
	   * mapping/billing/billingAccount_mapping_with_metrics_policies_custom.json
	   * Parent Object is a list, only Parent mapping file is provided by the Transformer
	   * child mapping has information about the fields (custom paths and transformations)
	   * inline mapping (parent mapping file has child mapping )
	   * custom mapping (json paths specified in the mapping file and transformations at the field level) for parent
	   * custom mapping for child objects
	   * One child object is an array (policies)
	   */

	  Object retObj = billingTransformerInlineCustom.transform(pLoad_parentList);

    assertNotNull(retObj);
            assertNotNull(((BillingAccount) ((List)retObj).get(0)).getMetrics());
            assertNotNull(((BillingAccount) ((List)retObj).get(0)).getPolicies());
            System.out.println("TransformedObject ----> " + retObj);
         
  }
  
  @Test
  public void testTransformation_nestedObjects_inline_list() throws Exception {
	  
	  /**
	   * mapping/billing/billingAccount_mapping_with_metrics_policies.json
	   * Parent Object is not a list, only Parent mapping file is provided by the Transformer
	   * child mapping has information about the class name, json matches with the pojo, no custom mapping
	   * inline mapping (parent mapping file has child mapping )
	   * custom mapping (json paths specified in the mapping file and transformations at the field level) for parent
	   * no custom mapping for child objects
	   * One child object is an array (policies)
	   */

	  Object retObj = billingTransformerInlineNoCustom.transform(pLoad);

    assertNotNull(retObj);
            assertNotNull(((BillingAccount) retObj).getMetrics());
            assertNotNull(((BillingAccount) retObj).getPolicies());
            System.out.println("TransformedObject ----> " + retObj);
         
  }
  
  @Test
  public void testTransformation_nestedObjects_inlineNoCustom_nestedCustom() throws Exception {
	  Object retObj = billingTransformerInlineNoCustomAndNestedNoInline.transform(pLoad);

	    assertNotNull(retObj);
	            assertNotNull(((BillingAccount) retObj).getMetrics());
	            assertNotNull(((BillingAccount) retObj).getPolicies());
	            System.out.println("TransformedObject ----> " + retObj);
	  
  }
  @Test
  public void testTransformation_nestedObjects_changeParent() throws Exception {
	  Object retObj = billingTransformerCustomChangeParent.transform(pLoad);

	    assertNotNull(retObj);
	            assertNotNull(((BillingAccount) retObj).getMetrics());
	            assertNotNull(((BillingAccount) retObj).getMetrics().getPolicies());
	            System.out.println("TransformedObject ----> " + retObj);
	  
  }
}
