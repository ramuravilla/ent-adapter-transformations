package com.amfam.ent.adapter.transform.core.customer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.amfam.ent.adapter.mapping.AdapterMappingConfigurer;
import com.amfam.ent.adapter.transform.core.Transformer;

@Component
public class CustomerSearchTransformer implements Transformer {
  private static Log logger = LogFactory.getLog(CustomerSearchTransformer.class);

  @Autowired AdapterMappingConfigurer aConfigurer;

  private final String mappingFileName;

  public CustomerSearchTransformer(
      @Value("${mapping.customer_contact_search_list_transformer}") String mappingFileName) {
    this.mappingFileName = mappingFileName;
  }
  
  public Object transform(String customerData) throws Exception {
    return transformData(customerData, mappingFileName);
  }

  @Override
  public AdapterMappingConfigurer aConfigurer() {
    // TODO Auto-generated method stub
    return aConfigurer;
  }
}
