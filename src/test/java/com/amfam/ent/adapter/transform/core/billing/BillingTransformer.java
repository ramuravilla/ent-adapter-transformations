package com.amfam.ent.adapter.transform.core.billing;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.amfam.ent.adapter.mapping.AdapterMappingConfigurer;
import com.amfam.ent.adapter.transform.core.Transformer;

@Component
public class BillingTransformer implements Transformer {
  private static Log logger = LogFactory.getLog(BillingTransformer.class);

  @Autowired AdapterMappingConfigurer aConfigurer;

  private final String mappingFileName;
  private final String mappingFileName_metrics;
  private final String mappingFileName_policies;

  public BillingTransformer(
      @Value("${mapping.bill_transformer}") String mappingFileName,
      @Value("${mapping.metrics_transformer}") String mappingFIleName_metrics,
      @Value("${mapping.policy_transformer}") String mappingFIleName_policies) {
    this.mappingFileName = mappingFileName;
    this.mappingFileName_metrics = mappingFIleName_metrics;
    this.mappingFileName_policies = mappingFIleName_policies;
  }

  /**
   * mapping/billing/billingAccount_mapping.json
   * mapping/billing/metrics_mapping.json
   * mapping/billing/policy_mapping.json
   * Parent Object is not a list, Parent and child mapping files are provided by the Transformer
   * child mapping has information about the parent class and field in the parent class to be assigned to
   * no inline mapping (parent mapping file has child mapping )
   * custom mapping (json paths specified in the mapping file and transformations at the field level)
   */
  public Object transform(String billData) throws Exception {
    return transformData(
        billData,
        new String[] {mappingFileName, mappingFileName_metrics, mappingFileName_policies});
  }

  @Override
  public AdapterMappingConfigurer aConfigurer() {
    // TODO Auto-generated method stub
    return aConfigurer;
  }
}
