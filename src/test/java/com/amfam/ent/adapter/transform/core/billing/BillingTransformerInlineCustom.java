package com.amfam.ent.adapter.transform.core.billing;

import com.amfam.ent.adapter.mapping.AdapterMappingConfigurer;
import com.amfam.ent.adapter.transform.core.Transformer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class BillingTransformerInlineCustom implements Transformer {
  private static Log logger = LogFactory.getLog(BillingTransformerInlineCustom.class);

  @Autowired AdapterMappingConfigurer aConfigurer;

  private final String mappingFileName;

  public BillingTransformerInlineCustom(
      @Value("${mapping.bill_transformer_with_metrics_policies_custom}") String mappingFileName) {
    this.mappingFileName = mappingFileName;
  }

  /**
   * mapping/billing/billingAccount_mapping_with_metrics_policies_custom.json
   * Parent Object is a list, only Parent mapping file is provided by the Transformer
   * child mapping has information about the fields (custom paths and transformations)
   * inline mapping (parent mapping file has child mapping )
   * custom mapping (json paths specified in the mapping file and transformations at the field level)
   */
  public Object transform(String billData) throws Exception {
    return transformData(billData, mappingFileName);
  }

  @Override
  public AdapterMappingConfigurer aConfigurer() {
    // TODO Auto-generated method stub
    return aConfigurer;
  }
}
