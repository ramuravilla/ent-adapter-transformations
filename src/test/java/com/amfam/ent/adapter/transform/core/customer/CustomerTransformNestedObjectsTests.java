package com.amfam.ent.adapter.transform.core.customer;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;

import com.amfam.ent.adapter.mapping.AdapterMappingConfigurer;
import com.amfam.ent.customer.reltio.adapter.mapping.model.CustomerSearchWrapper;

@SpringBootTest(
    classes = {
    		CustomerSearchTransformer.class,
      AdapterMappingConfigurer.class
      
    })
@ContextConfiguration
@AutoConfigureMockMvc(printOnlyOnFailure = false)
@EnableAutoConfiguration(
    exclude = {SecurityAutoConfiguration.class})
public class CustomerTransformNestedObjectsTests {

  @Autowired MockMvc mockMvc;
  

  @Autowired CustomerSearchTransformer customerSearchTransformer;

  private static Log logger = LogFactory.getLog(CustomerTransformNestedObjectsTests.class);

 
  String pLoad_customer_search = "{\n"
  		+ "	\"contacts\": [{\n"
  		+ "		\"contactId\": \"31C3AC0\",\n"
  		+ "		\"sourceSystemIdentifiers\": [{\n"
  		+ "			\"sourceSystemId\": \"c17a10a7-06de-4f8c-8211-83e2a6057f07\",\n"
  		+ "			\"sourceSystemName\": \"CAH\"\n"
  		+ "		}],\n"
  		+ "		\"firstName\": \"Talon\",\n"
  		+ "		\"middleName\": \"Q\",\n"
  		+ "		\"lastName\": \"McGlynn\",\n"
  		+ "		\"familySuffix\": null,\n"
  		+ "		\"professionalSuffix\": null,\n"
  		+ "		\"dateOfBirth\": null,\n"
  		+ "		\"deceased\": false,\n"
  		+ "		\"driversLicenseNumber\": \"W43592788500\",\n"
  		+ "		\"driversLicenseState\": \"WI\",\n"
  		+ "		\"addresses\": [{\n"
  		+ "			\"addressType\": \"HOME\",\n"
  		+ "			\"primaryIndicator\": true,\n"
  		+ "			\"addressLine1\": \"2795 Kristoffer Stravenue\",\n"
  		+ "			\"addressLine2\": null,\n"
  		+ "			\"state\": \"Wisconsin\",\n"
  		+ "			\"city\": \"Madison\",\n"
  		+ "			\"zipCode\": \"53713\",\n"
  		+ "			\"zip4Code\": null,\n"
  		+ "			\"zip2Code\": null,\n"
  		+ "			\"country\": \"United States\",\n"
  		+ "			\"longitude\": \"-158.3986\",\n"
  		+ "			\"latitude\": \"35.8404\"\n"
  		+ "		}],\n"
  		+ "		\"emails\": [{\n"
  		+ "			\"emailType\": null,\n"
  		+ "			\"emailAddress\": \"Jovanny78@yahoo.com\",\n"
  		+ "			\"primaryIndicator\": true\n"
  		+ "		}],\n"
  		+ "		\"phones\": [{\n"
  		+ "			\"phoneType\": \"Home\",\n"
  		+ "			\"phoneNumber\": \"866-562-5613\",\n"
  		+ "			\"primaryIndicator\": false,\n"
  		+ "			\"extension\": null,\n"
  		+ "			\"specialInstructions\": \"\"\n"
  		+ "		}]\n"
  		+ "	}, {\n"
  		+ "		\"contactId\": \"31B5556\",\n"
  		+ "		\"sourceSystemIdentifiers\": [{\n"
  		+ "			\"sourceSystemId\": \"77\",\n"
  		+ "			\"sourceSystemName\": \"CAH\"\n"
  		+ "		}],\n"
  		+ "		\"firstName\": \"Bulah\",\n"
  		+ "		\"middleName\": \"Q\",\n"
  		+ "		\"lastName\": \"McGlynn\",\n"
  		+ "		\"familySuffix\": null,\n"
  		+ "		\"professionalSuffix\": null,\n"
  		+ "		\"dateOfBirth\": \"1970-01-01\",\n"
  		+ "		\"deceased\": false,\n"
  		+ "		\"driversLicenseNumber\": \"W43592788500\",\n"
  		+ "		\"driversLicenseState\": \"WI\",\n"
  		+ "		\"addresses\": [{\n"
  		+ "			\"addressType\": \"HOME\",\n"
  		+ "			\"primaryIndicator\": true,\n"
  		+ "			\"addressLine1\": \"2063 Homenick Views\",\n"
  		+ "			\"addressLine2\": null,\n"
  		+ "			\"state\": \"Wisconsin\",\n"
  		+ "			\"city\": \"Madison\",\n"
  		+ "			\"zipCode\": \"53713\",\n"
  		+ "			\"zip4Code\": null,\n"
  		+ "			\"zip2Code\": null,\n"
  		+ "			\"country\": \"United States\",\n"
  		+ "			\"longitude\": \"78.7762\",\n"
  		+ "			\"latitude\": \"88.8145\"\n"
  		+ "		}],\n"
  		+ "		\"emails\": [{\n"
  		+ "			\"emailType\": null,\n"
  		+ "			\"emailAddress\": \"Lavinia7@hotmail.com\",\n"
  		+ "			\"primaryIndicator\": true\n"
  		+ "		}],\n"
  		+ "		\"phones\": [{\n"
  		+ "			\"phoneType\": \"Home\",\n"
  		+ "			\"phoneNumber\": \"244-988-3624\",\n"
  		+ "			\"primaryIndicator\": false,\n"
  		+ "			\"extension\": null,\n"
  		+ "			\"specialInstructions\": \"\"\n"
  		+ "		}]\n"
  		+ "	}],\n"
  		+ "	\"status\": {\n"
  		+ "		\"code\": 200,\n"
  		+ "		\"reason\": null,\n"
  		+ "		\"messages\": null,\n"
  		+ "		\"metrics\": {\n"
  		+ "			\"acceptedOn\": \"2023-01-08T03:27:51.898Z\",\n"
  		+ "			\"returnedOn\": \"2023-01-08T03:27:52.681Z\"\n"
  		+ "		},\n"
  		+ "		\"maxMessageLevel\": null,\n"
  		+ "		\"afe-trace-id\": \"trace\",\n"
  		+ "		\"afe-source-id\": \"source\"\n"
  		+ "	}\n"
  		+ "}";
  
  @Test
  public void testTransformation_nestedObjects() throws Exception {
	  
	  /**
	   * Parent Object is not a list,
	   * inline mapping i.e parent mapping file has child mappings
	   * custom mapping (json paths specified in the mapping file and transformations at the field level) for parent
	   * custom mapping for child objects
	   */

	  Object retObj = customerSearchTransformer.transform(pLoad_customer_search);

    assertNotNull(retObj);
    CustomerSearchWrapper cW = (CustomerSearchWrapper) retObj;
            assertNotNull(cW.getCustomerArrayList());
            
            
            assertNotNull(cW.getCustomerArrayList().get(0).getAddresses());
            assertNotNull(cW.getCustomerArrayList().get(1).getAddresses());
            assertNotNull(cW.getCustomerArrayList().get(0).getPhones());
            assertNotNull(cW.getCustomerArrayList().get(1).getPhones());
            assertNotNull(cW.getCustomerArrayList().get(0).getEmails());
            assertNotNull(cW.getCustomerArrayList().get(1).getEmails());
            System.out.println("TransformedObject ----> " + retObj);
         
  }
  
  
}
