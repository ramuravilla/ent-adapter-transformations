package com.amfam.ent.adapter.transform.core.billing;

import com.amfam.ent.adapter.mapping.AdapterMappingConfigurer;
import com.amfam.ent.adapter.transform.core.Transformer;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class BillingTransformerInlineNoCustom implements Transformer {
  private static Log logger = LogFactory.getLog(BillingTransformerInlineNoCustom.class);

  @Autowired AdapterMappingConfigurer aConfigurer;

  private final String mappingFileName;
  

  public BillingTransformerInlineNoCustom(
      @Value("${mapping.bill_transformer_with_metrics_policies}") String mappingFileName) {
    this.mappingFileName = mappingFileName;
    
  }
  /**
   * mapping/billing/billingAccount_mapping_with_metrics_policies.json
   * Parent Object is not a list, only Parent mapping file is provided by the Transformer
   * child mapping has information about the class name, json matches with the pojo, no custom mapping
   * inline mapping (parent mapping file has child mapping )
   * custom mapping (json paths specified in the mapping file and transformations at the field level) for parent
   * no custom mapping for child objects
   * One child object is an array (policies)
   */
  
  public Object transform(String billData) throws Exception {
    return transformData(
        billData,
        new String[] {mappingFileName});
  }

  @Override
  public AdapterMappingConfigurer aConfigurer() {
    // TODO Auto-generated method stub
    return aConfigurer;
  }
}
