package com.amfam.ent.adapter.transform.core.billing;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.amfam.ent.adapter.mapping.AdapterMappingConfigurer;
import com.amfam.ent.adapter.transform.core.Transformer;

@Component
public class BillingTransformerInlineNoCustomAndNestedNoInline implements Transformer {
  private static Log logger = LogFactory.getLog(BillingTransformerInlineNoCustomAndNestedNoInline.class);

  @Autowired AdapterMappingConfigurer aConfigurer;

  private final String mappingFileName;
//  private final String mappingFileName_metrics;
  private final String mappingFileName_policies;

  public BillingTransformerInlineNoCustomAndNestedNoInline(
      @Value("${mapping.bill_transformer_with_metrics}") String mappingFileName,
      @Value("${mapping.policy_transformer}") String mappingFIleName_policies) {
    this.mappingFileName = mappingFileName;
    this.mappingFileName_policies = mappingFIleName_policies;
  }

  public Object transform(String billData) throws Exception {
    return transformData(
        billData,
        new String[] {mappingFileName, mappingFileName_policies});
  }

  @Override
  public AdapterMappingConfigurer aConfigurer() {
    // TODO Auto-generated method stub
    return aConfigurer;
  }
}
