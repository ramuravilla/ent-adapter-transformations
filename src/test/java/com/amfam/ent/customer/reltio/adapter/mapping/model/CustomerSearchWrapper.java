package com.amfam.ent.customer.reltio.adapter.mapping.model;

import com.amfam.ent.model.customer.canonical.Customer;
import java.util.ArrayList;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class CustomerSearchWrapper {
  ArrayList<Customer> customerArrayList;
}
