package com.amfam.ent.model.billing.canonical;

import lombok.Data;

@Data
public class Policy {

  String policyNumber;

  String policyType;
  String underwritingCo;
  String lineOfBusiness;
  String partnerAccountId;
  Object policy;
  String remainingBillableMonths;
  String underwritingCompany;
  String primaryInsdFirstName;
  String primaryInsdLastName;
  String namedInsuredType;
}
