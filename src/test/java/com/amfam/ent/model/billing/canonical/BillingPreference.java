package com.amfam.ent.model.billing.canonical;

import lombok.Data;

@Data
public class BillingPreference {

  boolean isTPIEnabled;
  boolean isAutoPay;
  boolean isAutoPayEligible;
  boolean isPaperless;
}
