package com.amfam.ent.model.billing.canonical;

import lombok.Data;

@Data
public class BillingRole {

  String billingRoleDesc;
  String firstName;
  String lastName;
  String orgName;
}
