package com.amfam.ent.model.billing.canonical;

import java.math.BigDecimal;
import java.util.List;
import lombok.Data;

@Data
public class BillingAccount {

  String billingAccountId;
  String billingAccountName;
  Address billingAddress;
  String billingAccountStatus;
  String partnerId;
  BigDecimal accountBalance;
  BigDecimal minimumDue;
  BigDecimal creditBalance;
  BigDecimal fullPayBalance;
  String dueDate;
  String controllingProducer;
  Policy[] policies;
  List<BillingRole> billingRoles;
  List<BillingPreference> billingPreferences;
  String sourceSystem;
  String billingPlan;
  Metrics metrics;
}
