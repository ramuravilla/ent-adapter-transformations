package com.amfam.ent.model.billing.canonical;

import lombok.Data;

@Data
public class Address {

  String addressLine1;
  String addressLine2;
  String city;
  String stCd;
  String zip5;
  String zip4;
  String countryCd;
  String postalCd;
}
