package com.amfam.ent.model.billing.canonical;

import java.util.List;
import lombok.Data;

@Data
public class Metrics {

  private String acceptedOn;
  private String returnedOn;
  private List<Policy> policies;
}
