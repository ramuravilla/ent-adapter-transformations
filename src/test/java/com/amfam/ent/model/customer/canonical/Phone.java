package com.amfam.ent.model.customer.canonical;

import lombok.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Phone {

  private String phoneNumber;
  private String phoneType;
  private String extension;
  private String specialInstructions;
  private Boolean primaryIndicator;
}
