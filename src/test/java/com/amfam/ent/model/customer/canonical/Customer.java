package com.amfam.ent.model.customer.canonical;

import lombok.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Customer {

  private String partnerId;
  private String customerId;
  private String sourceSystemName;
  private String sourceSystemId;
  private String namePrefix;
  private String firstName;
  private String lastName;
  private String middleName;
  private String familySuffix;
  private String professionalSuffix;
  private String gender;
  private String dateOfBirth;
  private String maritalStatus;
  private Address[] addresses;
  private Email[] emails;
  private Phone[] phones;
  private String driversLicenseNumber;
  private String driversLicenseState;
  private String originalLicenseDate;
  private String socialSecurityNumber;
  private String spokenLanguage;
  private String employmentStatus;
  private Boolean deceasedIndicator;
}
