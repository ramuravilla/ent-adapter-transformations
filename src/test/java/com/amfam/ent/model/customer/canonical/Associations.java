package com.amfam.ent.model.customer.canonical;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Associations {

  // associations
  String associationType;
  String associationIdentifier;
  String associationSource;
}
