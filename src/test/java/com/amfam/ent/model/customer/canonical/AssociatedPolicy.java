package com.amfam.ent.model.customer.canonical;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AssociatedPolicy {

  String policyNumber;
  String partnerAccountId;
  String policyType;
  String effectiveDate;
  String expiryDate;
  String cancelDate;
  Role role;
  String policyStatus;
  String totalPremium;
  Address policyAddress;
  String servicingProducerId;
  String servicingProducerCode;
  String billAccountNumber;
  String sourceSystem;
}
