package com.amfam.ent.model.customer.canonical;

import lombok.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Role {

  private String entCustomerId;
  private String localCustomerId;
  private String roleType;
}
