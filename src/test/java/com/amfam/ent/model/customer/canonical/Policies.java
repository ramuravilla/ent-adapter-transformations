package com.amfam.ent.model.customer.canonical;

import lombok.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Policies {

  private String policyNumber;
  private String partnerAccountId;
  private String policyType;
  private String productLine;
  private String underWritingCompany;
  private String effectiveDate;
  private String expiryDate;
  private String cancelDate;
  private Role[] roles;
  private String policyStatus;
  private String totalPremium;
  private Address[] policyMailingAddress;
  private String producerCode;
  private String billAccountNumber;
  private String sourceSystem;
}
