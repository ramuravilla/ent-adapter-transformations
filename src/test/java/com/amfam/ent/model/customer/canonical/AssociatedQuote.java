package com.amfam.ent.model.customer.canonical;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AssociatedQuote {

  String quoteNumber;
  String partnerAccountId;
  String quoteType;
  String effectiveDate;
  Role role;
  String quoteStatus;
  String producerId;
  String producerCode;
  String sourceSystem;
}
