package com.amfam.ent.model.customer.canonical;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Quotes {

  private String quoteNumber;
  private String partnerAccountId;
  private String quoteType;
  private String productLine;
  private String underWritingCompany;
  private String effectiveDate;
  private Role[] roles;
  private String quoteStatus;
  private String producerId;
  private String producerCode;
  private String sourceSystemName;
}
