package com.amfam.ent.model.customer.canonical;

import lombok.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Address {

  private String addressLine1;
  private String addressLine2;
  private String city;
  private String state;
  private String zipCode;
  private String zip4Code;
  private String country;
  private String latitude;
  private String longitude;
  private String addressType;
  Boolean primaryIndicator;
}
