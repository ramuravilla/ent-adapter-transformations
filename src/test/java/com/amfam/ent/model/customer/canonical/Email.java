package com.amfam.ent.model.customer.canonical;

import lombok.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Email {

  private String emailAddress;
  private String emailType;
}
