package com.amfam.ent.billing.adapter.guidewire.transform.spel;

import com.amfam.ent.adapter.transform.core.SPELTransformer;
import com.amfam.ent.core.utils.DateUtils;
import org.springframework.stereotype.Component;

/**
 * make sure the component name mentioned here matches with the @ name specified in the mapping file
 *
 * @author RXR130
 */
@Component("dueDateTransformer")
public class DateTransformer implements SPELTransformer {

  @Override
  public String transform(String val) {
    // TODO Auto-generated method stub
    String transformedVal = DateUtils.formatDateIso8601(DateUtils.parseDateMMDDYYYY(val));
    return transformedVal;
  }
}
